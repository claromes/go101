package main

import (
	"fmt"
	"strings"
)

func main() {
	_first, _last := "Go", "Lang"
	_age := 11

	fmt.Println("Name:", "\t", _first, _last, "\nAge:", "\t",_age, len(_first))
	fmt.Printf("Name: %s %s\nAge: %d\n", strings.ToUpper(_first), strings.ToLower(_last), _age)

}