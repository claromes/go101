package main

import "fmt"

func main() {
	/*
	var colors [5]string

	colors[0] = "blue"
	colors[1] = "green"
	colors[2] = "yellow"
	colors[3] = "red"
	colors[4] = "orange"
	*/

	colors := [...]string {"blue", "green", "yellow", "red", "orange"}

	fmt.Println("Length:", len(colors))
	fmt.Println("First color:", colors[0])
	fmt.Println("Last color:", colors[len(colors) -1])
}