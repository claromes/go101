package main

import "fmt"

func main() {
	fmt.Print("Função Print()")
	fmt.Println("Função Println() com \"escape\"!")
	fmt.Println("Função", "Println()")
}