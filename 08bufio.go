package main

import (
	"fmt"
	"bufio"
	"os"
)

func main() {
	_input := bufio.NewReader(os.Stdin)

	fmt.Print("First name? ")
	_first, _ := _input.ReadString('\n')

	fmt.Print("Last name? ")
	_last, _ := _input.ReadString('\n')

	fmt.Print("Age? ")
	_age, _ := _input.ReadString('\n')

	fmt.Printf("First name: %sLast name: %sAge: %s", _first, _last, _age)
}