package main

import "fmt"

func main() {
	var _int int = 10
	var _str string = "String!"
	var _no_type = "String sem tipo!"

	_dec := 10.5
	_bool := true //default false

	fmt.Println(_int, _str, _no_type, _dec, _bool)
}